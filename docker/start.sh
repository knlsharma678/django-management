#!/usr/bin/env bash
python3 --version
echo "command started and version printed"
python3 app/manage.py makemigrations
echo "make migrations" 
python3 app/manage.py migrate
echo "migrate"   
python3 app/manage.py runserver 0.0.0.0:8000
echo "server up ..."

from django.shortcuts import render, redirect
from .forms import EmployeeForm
from .models import Employee
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db.models import Q

from login.models import Login

# Create your views here.

@login_required
def employee_list(request):
    context = {'employee_list': Employee.objects.all()}
    return render(request, "employee_register/employee_list.html", context)


@login_required
def employee_form(request, id=0):
    if request.method == "GET":
        if id == 0:
            form = EmployeeForm()
        else:
            employee = Employee.objects.get(pk=id)
            form = EmployeeForm(instance=employee)
        return render(request, "employee_register/employee_form.html", {'form': form})
    else:
        if id == 0:
            form = EmployeeForm(request.POST)
        else:
            employee = Employee.objects.get(pk=id)
            form = EmployeeForm(request.POST,instance= employee)
        if form.is_valid():
            form.save()
        return redirect('/home/book/list')

@login_required
def employee_delete(request,id):
    employee = Employee.objects.get(pk=id)
    employee.delete()
    return redirect('/home/book/list')

def search(request):
    if request.method=='POST':
        srch = request.POST['srh']
        print(srch)
        if srch:
            match1 = Login.objects.filter( Q(name__iexact=srch))
            print(match1)                                         

            if match1:
                return render(request, 'employee_register/search.html', {'form_login': match1})

            elif match1 is not None:
                match = Employee.objects.filter( Q(Bookname__iexact=srch) |
                                         Q(Authorname__iexact=srch)                                                   
                                                   )

                print(match)
                return render(request, 'employee_register/search.html', {'form': match}) 

            elif srch:
                messages.error(request , 'no result found')
        
            else:
                messages.error(request , 'no result found')          
        else:
            messages.error(request , 'no result found') 
            return redirect('/home/book/search')        
                    
    return render(request ,'employee_register/search.html')

      




    
